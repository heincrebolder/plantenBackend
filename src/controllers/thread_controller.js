const Thread = require('../models/thread')

function getAll(req, res) {
    Thread.find({}, {
            __v: 0
        })        
        .populate({
            path: 'plant',
            populate:{path:'climate', model: 'climate'}, 
        })
        .populate('user', 'name')
        .exec()
        .then(threads => {
            res.status(200).send(threads)
        })
};

function getOne(req, res) {
    Thread.findOne({
            _id: req.headers._id
        })
        .then(thread => {
            if (thread === null) {
                res.status(401).send({
                    Error: 'thread does not exist'
                })
            } else {
                res.status(200).send(thread);
                console.log('>>thread returned')
            }
        })
};

function getOneById(req, res) {
    Thread.findById(req.params.id)
        .populate({
            path: 'plant', 
            populate:{path: 'user', model: 'user'}, 
        })
        .exec()
        .then(thread => {
            if (thread === null) {
                res.status(401).send({
                    Error: 'thread does not exist.'
                })
            } else {
                res.status(200).send(thread);
                console.log('>>thread returned');
            }
        })
};


function create(req, res) {
    Thread.create({
            title: req.body.title,
            text: req.body.text,
            imageLink: req.body.imageLink,
            plant: req.body.plant,
            user: req.body.user
        })
        .then(() =>
            res.status(200).send({
                Message: "thread created succesfully"
            }), console.log('>>thread created'))
        .catch((err) => {
            if (err.name == 'MongoError' && err.code == 11000) {
                res.status(401).send({
                    Error: 'An thread with this title already exists.'
                });
            } else {
                res.status(401).send({
                    err
                });
            }
        });
};

function edit(req, res) {
    Thread.findOne({
        _id: req.body._id
    })
    .then(thread => {
        if( thread === null){
            res.status(401).send({
                Error: "Thread does not exist"
            })
        } else {
            let titleToSet = req.body.title;
            let textToSet = req.body.text;
            let imageLinkToSet = req.body.imageLink;
            let plantToSet = req.body.imageLink;

            if (req.body.title === '' || req.body.title === null) titleToSet = thread.title;
            if (req.body.text === '' || req.body.text === null) textToSet = thread.text;
            if (req.body.imageLink === '' || req.body.imageLink === null) imageLinkToSet = thread.imageLink;
            if (req.body.plant === '' || req.body.plant === null) plantToSet = thread.plant;

            thread.set({
                title: titleToSet,
                text: textToSet,
                imageLink: imageLinkToSet,
                plant: plantToSet
            })
            thread.save()
                .then(() =>{
                    res.status(200).send({
                        Message: 'thread edited successfully'
                    })
                    console.log('>>>thread edited')
                })
                .catch((err) => res.status(401).send({ err}));
        }
    })
};

function remove(req, res) {
    Thread.findById(req.params.id)
        .then(thread => {
            if (thread === null) {
                res.status(401).send({
                    Error: 'thread does not exist.'
                })
            } else {
                thread.delete()
                    .then(() => {
                        res.status(200).send({
                            Message: 'thread succesfully removed.'
                        })
                        console.log('>>>thread removed')
                    });
            }
        });
};

module.exports = {
    getAll,
    getOne,
    getOneById,
    create,
    edit,
    remove
}