const UserController = require('../src/controllers/user_controller');
const AuthController = require('../src/controllers/auth_controller');
const PlantController = require('../src/controllers/plant_controller');
const ClimateController = require('../src/controllers/climate_controller');
const ThreadController = require('../src/controllers/thread_controller');

module.exports = (app) => {
    //
    //User routes
    //
    //create a new user with 'name, password'
    app.post('/api/user/register', UserController.create);
    //create a token with 'name, password'
    app.post('/api/user/login', AuthController.login);
    //change password of an existing user with 'name, password, newPassword'
    app.put('/api/user/:id', AuthController.validateToken, UserController.edit);
    //remove a user from the database with 'name, password'
    app.delete('/api/user/', AuthController.validateToken, UserController.remove);

    //
    //Plant routes
    //
    //create new plant with name, family, description, waterneed, sunneed
    app.get('/api/plant/', PlantController.getAll);
    app.get('/api/plant/:id', PlantController.getOneById)
    app.post('/api/plant/', AuthController.validateToken, PlantController.create);
    app.put('/api/plant/:id',AuthController.validateToken, PlantController.edit);
    app.delete('/api/plant/:id', AuthController.validateToken, PlantController.remove);


    //
    //Climate routes
    //
    //get all climates
    app.get('/api/climate/', ClimateController.getAll);
    app.get('/api/climate/:id', ClimateController.getOneById);
    //create new climate with name, averageTemp, humidity
    app.post('/api/climate/', AuthController.validateToken, ClimateController.create);
    app.put('/api/climate/:id', AuthController.validateToken, ClimateController.edit);
    app.delete('/api/climate/:id',AuthController.validateToken, ClimateController.remove);

    //
    //Thread routes
    //
    app.get('/api/thread/', ThreadController.getAll);
    app.get('/api/thread/:id', ThreadController.getOneById);
    app.post('/api/thread/', AuthController.validateToken, ThreadController.create);
    app.put('/api/thread/:id', AuthController.validateToken, ThreadController.edit);
    app.delete('/api/thread/:id', AuthController.validateToken, ThreadController.remove);

};